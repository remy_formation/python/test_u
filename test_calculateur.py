from calculateur import *
import pytest

def test_addition():
    result = addition(2,3)
    assert result == 5

def test_multiplication():
    result = multiplication(2,3)
    assert result == 6
